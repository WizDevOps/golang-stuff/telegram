module gitlab.com/WizDevOps/golang-stuff/telegram

go 1.12

require (
	github.com/json-iterator/go v1.1.7
	github.com/kirillDanshin/dlog v0.0.0-20170728000807-97d876b12bf9
	github.com/kirillDanshin/myutils v0.0.0-20160713214838-182269b1fbcc // indirect
	github.com/klauspost/compress v1.7.4 // indirect
	github.com/klauspost/cpuid v1.2.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/valyala/fasthttp v1.4.0
	golang.org/x/text v0.3.2
)
