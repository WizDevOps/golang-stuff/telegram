# GoLang bindings for the Telegram API
> This package is in active development and **not recommended for use in production for now**. Use [go dep](https://golang.github.io/dep/) to freeze the state of the package in your projects!

## Start using telegram
Download and install it:  
`$ go get -u gitlab.com/WizDevOps/golang-stuff/telegram`

Import it in your code:  
`import "gitlab.com/WizDevOps/golang-stuff/telegram"`

## Need help?
- [Open new issue](https://gitlab.com/WizDevOps/golang-stuff/telegram/issues/new)
- [Discuss in Discord](https://discord.gg/dCWkgSS)