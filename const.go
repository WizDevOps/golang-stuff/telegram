package telegram

// Version represents current version of Telegram API supported by this package
const Version string = "4.4"

// Action represents available and supported status actions of bot
const (
	ActionFindLocation    string = "find_location"
	ActionRecordAudio     string = "record_audio"
	ActionRecordVideo     string = "record_video"
	ActionRecordVideoNote string = "record_video_note"
	ActionTyping          string = "typing"
	ActionUploadAudio     string = "upload_audio"
	ActionUploadDocument  string = "upload_document"
	ActionUploadPhoto     string = "upload_photo"
	ActionUploadVideo     string = "upload_video"
	ActionUploadVideoNote string = "upload_video_note"
)

// Chat represents available and supported chat types
const (
	ChatChannel    string = "channel"
	ChatGroup      string = "group"
	ChatPrivate    string = "private"
	ChatSuperGroup string = "supergroup"
)

// Command represents global commands which should be supported by any bot.
// You can user IsCommandEqual method of Message for checking.
//
// See: https://core.telegram.org/bots#global-commands
const (
	CommandStart    string = "start"
	CommandHelp     string = "help"
	CommandSettings string = "settings"
)

// Entity represents available and supported entity types
const (
	EntityBold        string = "bold"
	EntityBotCommand  string = "bot_command"
	EntityCashtag     string = "cashtag"
	EntityCode        string = "code"
	EntityEmail       string = "email"
	EntityHashtag     string = "hashtag"
	EntityItalic      string = "italic"
	EntityMention     string = "mention"
	EntityPhoneNumber string = "phone_number"
	EntityPre         string = "pre"
	EntityTextLink    string = "text_link"
	EntityTextMention string = "text_mention"
	EntityURL         string = "url"
)

// Method represents available and supported Telegram API methods
const (
	MethodAddStickerToSet         string = "addStickerToSet"
	MethodAnswerCallbackQuery     string = "answerCallbackQuery"
	MethodAnswerInlineQuery       string = "answerInlineQuery"
	MethodAnswerPreCheckoutQuery  string = "answerPreCheckoutQuery"
	MethodAnswerShippingQuery     string = "answerShippingQuery"
	MethodCreateNewStickerSet     string = "createNewStickerSet"
	MethodDeleteChatPhoto         string = "deleteChatPhoto"
	MethodDeleteChatStickerSet    string = "deleteChatStickerSet"
	MethodDeleteMessage           string = "deleteMessage"
	MethodDeleteStickerFromSet    string = "deleteStickerFromSet"
	MethodDeleteWebhook           string = "deleteWebhook"
	MethodEditMessageCaption      string = "editMessageCaption"
	MethodEditMessageLiveLocation string = "editMessageLiveLocation"
	MethodEditMessageMedia        string = "editMessageMedia"
	MethodEditMessageReplyMarkup  string = "editMessageReplyMarkup"
	MethodEditMessageText         string = "editMessageText"
	MethodExportChatInviteLink    string = "exportChatInviteLink"
	MethodForwardMessage          string = "forwardMessage"
	MethodGetChat                 string = "getChat"
	MethodGetChatAdministrators   string = "getChatAdministrators"
	MethodGetChatMember           string = "getChatMember"
	MethodGetChatMembersCount     string = "getChatMembersCount"
	MethodGetFile                 string = "getFile"
	MethodGetGameHighScores       string = "getGameHighScores"
	MethodGetMe                   string = "getMe"
	MethodGetStickerSet           string = "getStickerSet"
	MethodGetUpdates              string = "getUpdates"
	MethodGetUserProfilePhotos    string = "getUserProfilePhotos"
	MethodGetWebhookInfo          string = "getWebhookInfo"
	MethodKickChatMember          string = "kickChatMember"
	MethodLeaveChat               string = "leaveChat"
	MethodPinChatMessage          string = "pinChatMessage"
	MethodPromoteChatMember       string = "promoteChatMember"
	MethodRestrictChatMember      string = "restrictChatMember"
	MethodSendAnimation           string = "sendAnimation"
	MethodSendAudio               string = "sendAudio"
	MethodSendChatAction          string = "sendChatAction"
	MethodSendContact             string = "sendContact"
	MethodSendDocument            string = "sendDocument"
	MethodSendGame                string = "sendGame"
	MethodSendInvoice             string = "sendInvoice"
	MethodSendLocation            string = "sendLocation"
	MethodSendMediaGroup          string = "sendMediaGroup"
	MethodSendMessage             string = "sendMessage"
	MethodSendPhoto               string = "sendPhoto"
	MethodSendPoll                string = "sendPoll"
	MethodSendSticker             string = "sendSticker"
	MethodSendVenue               string = "sendVenue"
	MethodSendVideo               string = "sendVideo"
	MethodSendVideoNote           string = "sendVideoNote"
	MethodSendVoice               string = "sendVoice"
	MethodSetChatDescription      string = "setChatDescription"
	MethodSetChatPermissions      string = "setChatPermissions"
	MethodSetChatPhoto            string = "setChatPhoto"
	MethodSetChatStickerSet       string = "setChatStickerSet"
	MethodSetChatTitle            string = "setChatTitle"
	MethodSetGameScore            string = "setGameScore"
	MethodSetPassportDataErrors   string = "setPassportDataErrors"
	MethodSetStickerPositionInSet string = "setStickerPositionInSet"
	MethodSetWebhook              string = "setWebhook"
	MethodStopMessageLiveLocation string = "stopMessageLiveLocation"
	MethodStopPoll                string = "stopPoll"
	MethodUnbanChatMember         string = "unbanChatMember"
	MethodUnpinChatMessage        string = "unpinChatMessage"
	MethodUploadStickerFile       string = "uploadStickerFile"
)

// Mode represents available and supported parsing modes of messages
const (
	StyleHTML     string = "html"
	StyleMarkdown string = "markdown"
)

// Mime represents available and supported MIME types of data
const (
	MimeHTML string = "text/html"
	MimeMP4  string = "video/mp4"
	MimePDF  string = "application/pdf"
	MimeZIP  string = "application/zip"
)

// Scheme represents optional schemes for URLs
const (
	SchemeAttach   string = "attach"
	SchemeTelegram string = "tg"
)

// Status represents available and supported statuses of ID
const (
	StatusAdministrator string = "administrator"
	StatusCreator       string = "creator"
	StatusKicked        string = "kicked"
	StatusLeft          string = "left"
	StatusMember        string = "member"
	StatusRestricted    string = "restricted"
)

// Type represents available and supported types of data
const (
	TypeAddress               string = "address"
	TypeArticle               string = "article"
	TypeAudio                 string = "audio"
	TypeBankStatement         string = "bank_statement"
	TypeContact               string = "contact"
	TypeDocument              string = "document"
	TypeDriverLicense         string = "driver_license"
	TypeEmail                 string = "email"
	TypeGame                  string = "game"
	TypeGIF                   string = "gif"
	TypeIdentityCard          string = "identity_card"
	TypeInternalPassport      string = "internal_passport"
	TypeLocation              string = "location"
	TypeMpeg4Gif              string = "mpeg4_gif"
	TypePassport              string = "passport"
	TypePassportRegistration  string = "passport_registration"
	TypePersonalDetails       string = "personal_details"
	TypePhoneNumber           string = "phone_number"
	TypePhoto                 string = "photo"
	TypeRentalAgreement       string = "rental_agreement"
	TypeSticker               string = "sticker"
	TypeTemporaryRegistration string = "temporary_registration"
	TypeUtilityBill           string = "utility_bill"
	TypeVenue                 string = "venue"
	TypeVideo                 string = "video"
	TypeVoice                 string = "voice"
)

// Update represents available and supported types of updates
const (
	UpdateCallbackQuery      string = "callback_query"
	UpdateChannelPost        string = "channel_post"
	UpdateChosenInlineResult string = "chosen_inline_result"
	UpdateEditedChannelPost  string = "edited_channel_post"
	UpdateEditedMessage      string = "edited_message"
	UpdateInlineQuery        string = "inline_query"
	UpdateMessage            string = "message"
	UpdatePoll               string = "poll"
	UpdatePreCheckoutQuery   string = "pre_checkout_query"
	UpdateShippingQuery      string = "shipping_query"
)
